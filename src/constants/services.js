import React from "react"
import { FaCertificate, FaReact, FaNode, FaCubes } from "react-icons/fa"

export default [
  {
    icon: <FaCertificate />,
    title: "Certified",
    text:
      "University of Washington Continuum College educated in JavaScript, HTML5 & CSS3",
  },
  {
    icon: <FaReact />,
    title: "React",
    text:
      "Front-end experience with React using APIs, Firebase and testing with Enzyme",
  },
  {
    icon: <FaNode />,
    title: "Node.js",
    text:
      "Back-end experience with Node using Express, JSON Web Tokens,  Mongoose and  Bcrypt",
  },
  {
    icon: <FaCubes />,
    title: "Product Owner",
    text:
      "P.O. for web application built on Agile/JIRA processes, React, Node, Python, Docker and BDD testing using Gherkin",
  },
]
